import sys
impl_path = '..\\implementation'
if impl_path not in sys.path:
	sys.path.append(impl_path)

import PKI



print('Generating RSA keys longer than 1024-bits might take long.')

theForce = PKI.Identity(
	name="The Brute Force root CA", 
	person="legal", 
	address="the Galaxy, far far away",
	moreInfo="May the brute force be with you."
	)
theForce = PKI.CertificationAuthority(whoami=theForce)
bitlength = 3072
print("Generating {}-bit RSA key pair for the root CA:\n{}\n".format(bitlength, theForce.whoami))
theForce.getNewKeyAndCert(None, PKI.RSA, bitlength, 'SHA512')

jedi = PKI.Identity(
	name="The Jedi Council",
	person="legal",
	address="the Galaxy, far far away"
	)
jedi = PKI.CertificationAuthority(whoami=jedi)
bitlength = 2048
print("Generating {}-bit RSA key pair for a subordinate CA:\n{}\n".format(bitlength, jedi.whoami))
jedi.getNewKeyAndCert(theForce, PKI.RSA, bitlength, 'SHA512')

darkSide = PKI.Identity(
	name="The Dark Side of the Force",
	person="legal",
	address="the Galaxy, far far away",
	moreInfo="Join us! We've got cookies."
	)
darkSide = PKI.CertificationAuthority(whoami=darkSide)
bitlength = 2048
print("Generating {}-bit RSA key pair for a subordinate CA:\n{}\n".format(bitlength, darkSide.whoami))
darkSide.getNewKeyAndCert(theForce, PKI.RSA, bitlength, 'SHA512')



quiGonJinn = PKI.Identity(
	name="Qui Gon Jinn",
	person="natural",
	moreInfo="jedi master"
	)
quiGonJinn = PKI.Entity(whoami=quiGonJinn)
bitlength = 1024
print("Generating {}-bit RSA key pair for an end user:\n{}\n".format(bitlength, quiGonJinn.whoami))
quiGonJinn.getNewKeyAndCert(jedi, PKI.RSA, bitlength, 'SHA256')

obiWanKenobi = PKI.Identity(
	name="Obi-Wan Kenobi",
	person="natural",
	moreInfo="padawan"
	)
obiWanKenobi = PKI.Entity(whoami=obiWanKenobi)
bitlength = 512
print("Generating {}-bit RSA key pair for an end user:\n{}\n".format(bitlength, obiWanKenobi.whoami))
obiWanKenobi.getNewKeyAndCert(jedi, PKI.RSA, bitlength, 'SHA256')

anakinSkywalker = PKI.Identity(
	name="Anakin Skywalker",
	person="natural",
	address="Tatooine")

message = "The force is strong with {}".format(PKI.make_repr([[0, 'this one', anakinSkywalker]]))
message = quiGonJinn.signData(message, PKI.SignedMessage)
print(PKI.make_repr([[1, 'message', message]]))
print('Validation result including the chain: {}'.format(PKI.validator.validateSignedDataWithChain(message)))
print('This should be completely valid.\n')

message = "Darth Maul killed my master and I killed Darth Maul. The score is 1:1."
message = obiWanKenobi.signData(message, PKI.SignedMessage)
print(PKI.make_repr([[1, 'message', message]]))
print('Validation result including the chain: {}'.format(PKI.validator.validateSignedDataWithChain(message)))
print('This should be completely valid.\n')

message = "What? I'm perfectly healthy !!!"
message = PKI.forgery.forgeMessage(message, quiGonJinn.cert, quiGonJinn.privateKey)
print(PKI.make_repr([[1, 'message', message]]))
print('Validation result including the chain: {}'.format(PKI.validator.validateSignedDataWithChain(message)))
print('This shouldn\'t be valid.')
print('Qui Gonn\'s certificate and its chain are ok, but the signature of the message is corrupt.\n')



yoda = PKI.Identity(
	name="Yoda the eary",
	person="natural",
	address="Dagobah",
	moreInfo="master"
	)
yoda = PKI.Entity(whoami=yoda)
bitlength = 1024
print("Generating {}-bit RSA key pair for the real Yoda.\n".format(bitlength))
yoda.getNewKeyAndCert(jedi, PKI.RSA, bitlength, 'MD5')
bitlength = 666
print("Generating {}-bit RSA key pair for a fake Yoda.\n".format(bitlength))
fakeYoda = PKI.Entity(cert=yoda.cert, privateKey=PKI.RSA.getNewKey(bitlength))
fakeYoda.cert = PKI.forgery.forgeCertificate(yoda.cert.data, jedi.cert, fakeYoda.privateKey)

message = "Why was five afraid of seven?\nBecause 6 7 8."
message = fakeYoda.signData(message, PKI.SignedMessage)
print(PKI.make_repr([[1, 'message', message]]))
print('Validation result including the chain: {}'.format(PKI.validator.validateSignedDataWithChain(message)))
print('The message was signed using the private key associated with the certificate.')
print('However, the end-user certificate was not signed with the certificate of the claimed subordinate CA.')
print('The rest of the chain is ok.\n')
