import signature_object



class Signature_RSA(signature_object.Signature):
	'''
		A container for RSA signature
	'''

	def __new__(cls, sign_val):
		if not isinstance(sign_val, (int, long)):
			raise TypeError('\'{}\' is not a valid RSA signature value'.format(sign_val))
		return super(Signature_RSA, cls).__new__(cls, long(sign_val))
