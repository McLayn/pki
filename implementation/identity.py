from _utils_format import *
from _utils_hash import *
import collections
import re



dataLabels = ["name", "address", "email", "web", "person", "moreInfo"]
ID_Data = collections.namedtuple("ID_Data", dataLabels)



class Identity(ID_Data):
	'''
		immutable iff all fields are immutable
	'''
	
	def __new__(cls, name, person, address='', email='', web='', moreInfo=''):
		def checkString(s):
			if not isinstance(s, (str, unicode)):
				raise TypeError('\'{}\' is not a string'.format(s))

		checkString(name)
		name = name.strip()
		if (name == ''):
			raise ValueError('Printed name cannot be empty.')

		checkString(address)

		checkString(email)
		if (email != ''):
			if (len(re.findall(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", email)) == 0):
				raise ValueError('\'{}\' is not a valid email'.format(email))

		checkString(web)
		if (web != ''):
			if (len(re.findall('(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', web)) == 0):
				raise ValueError('\'{}\' is not a valid web address'.format(web))

		checkString(person)
		if not person in ['natural', 'legal']:
			raise ValueError('\'{}\' is not one of: \'natural\', \'legal\''.format(person))			
		
		checkString(moreInfo)

		return super(Identity, cls).__new__(cls, name, address, email, web, person, moreInfo)


	def __repr__(self):
		fields = []
		for item in dataLabels:
			value = self._asdict()[item]
			if (value != ''):
				fields.append((0, item, value))
		return make_repr(fields)

	def __hash__(self):
		return doHash(self.__repr__())
