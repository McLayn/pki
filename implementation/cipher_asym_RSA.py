from _utils_math import *
import key_asym_RSA
import cipher_asym
import signature_RSA

class RSA(cipher_asym.AsymmetricCipher):
	'''
		RSA implementation
	'''

	@staticmethod
	def checkMessage(message, publicKey):
		'''
			RSA operates on numbers
		'''
		if not isinstance(message, (int, long)):
			raise TypeError('message=\'{}\' is not a number'.format(message))
		if (message < 0):
			raise ValueError('Message is not a positive number.')
		if (publicKey.fields.N <= message):
			raise ValueError('Message is greater than modulus.')

	@staticmethod
	def checkPublicKey(key):
		'''
			Raise an error when <key> is not RSA public key
		'''
		if not isinstance(key, key_asym_RSA.RSA_PublicKey):
			raise TypeError('\'{}\' is not a valid public key'.format(key))

	@staticmethod
	def checkPrivateKey(key):
		'''
			Raise an error when <key> is not RSA private key
		'''
		if not isinstance(key, key_asym_RSA.RSA_PrivateKey):
			raise TypeError('\'{}\' is not a valid private key'.format(key))

	# Encryption / Decryption:

	@staticmethod
	def encrypt(message, publicKey):
		RSA.checkMessage(message, publicKey)
		RSA.checkPublicKey(publicKey)
		return pow(message, publicKey.fields.e, publicKey.fields.N)

	@staticmethod
	def decrypt(message, privateKey):
		RSA.checkMessage(message, privateKey.publicKey)
		RSA.checkPrivateKey(privateKey)
		return pow(message, privateKey.fields.d, privateKey.publicKey.fields.N)

	# Signature / Validation:

	@staticmethod
	def sign(message, privateKey):
		message %= privateKey.publicKey.fields.N
		return signature_RSA.Signature_RSA(RSA.decrypt(message, privateKey))

	@staticmethod
	def validateSignature(message, publicKey, signature):
		message %= publicKey.fields.N
		RSA.checkMessage(message, publicKey)
		if (publicKey.fields.N <= signature.signature_value):
			return False
		m = RSA.encrypt(signature.signature_value, publicKey)
		return (m == message)

	@staticmethod
	def corruptSignature(signatureValue):
		return signature_RSA.Signature_RSA(signatureValue.signature_value ^ 42)

	# Generating keys:

	@staticmethod
	def getNewKey(bitlength):
		'''
			Generate a new RSA key
			Minimum bitlength is 32 bits
		'''
		if not isinstance(bitlength, (int, long)):
			raise TypeError('\'{}\' is not a number')
		if (bitlength < 32):
			raise ValueError('\'{}\' is too small')
		e = 65537
		d = None
		while (d == None):
			p = getLargePrime(bitlength / 2)
			q = getLargePrime(bitlength / 2)
			d = invmod(e, lcm(p - 1, q - 1))
		N = p * q

		pub = key_asym_RSA.RSA_PublicKey(e, N)
		priv = key_asym_RSA.RSA_PrivateKey(d, pub)
		return priv
