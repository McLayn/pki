import collections
import certificate



Certificate_Response  = collections.namedtuple("Certificate_Response", ["cert", "info"])



class CertResponse(Certificate_Response):
	'''
		Represents a response to certificate request.
		Failed responses have <cert>=None

		immutable iff
		- <cert> is immutable
	'''

	_authorisedUse = object()

	def __new__(cls, cert, info, locked):
		'''
			Do not use!
			Use static methods newSuccessResponse(cert) and newFailedResponse(reason) instead.
		'''	
		assert locked is CertResponse._authorisedUse
		if not (cert is None):
			if not isinstance(cert, certificate.Certificate):
				raise TypeError('\'{}\' is not a valid certificate'.format(cert))
		info = str(info)
		return super(CertResponse, cls).__new__(cls, cert, info)

	def isFailed(self):
		'''
			return whether this response is a failed one
		'''
		return (self.cert is None)

	@staticmethod
	def newSuccessResponse(cert):
		'''
			return successfull response containting:
				cert = <cert>
				info = 'OK'
		'''
		return CertResponse(cert, 'OK', CertResponse._authorisedUse)

	@staticmethod
	def newFailedResponse(reason):
		'''
			return failed response containting:
				cert = None
				info = 'ERROR: ' + reason
		'''
		return CertResponse(None, 'ERROR: ' + str(reason), CertResponse._authorisedUse)
