import certificate
import signed_data
import signed_message



# creates corrupt signed objects

def forgeSignedData(data, cert, privKey, resultClass=signed_data.SignedData):
	signatureValue = privKey.hashAndSign(data, cert.data.hashFun)
	signatureValue = privKey.publicKey.cryptosystem.corruptSignature(signatureValue)
	return resultClass(data, cert, signatureValue)

def forgeCertificate(data, ca_cert, privKey):
	data = certificate.Cert_Data(data.serial, data.subject, privKey.publicKey, data.hashFun)
	return forgeSignedData(data, ca_cert, privKey, certificate.Certificate)

def forgeMessage(data, cert, privKey):
	return forgeSignedData(data, cert, privKey, signed_message.SignedMessage)
