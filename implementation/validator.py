import key_asym
import signed_data
import certificate



def validateHashSignature(hashValue, signature, pubKey):
	'''
		return whether <signature> is <hashValue> signed using the key corresponding to <pubKey>
	'''
	if not isinstance(hashValue, (int, long)):
		raise TypeError('\'{}\' is not a valid hash value'.format(hashValue))
	if not isinstance(pubKey, key_asym.Key_Public):
		raise TypeError('\'{}\' is not a valid public key'.format(pubKey))
	return pubKey.validateSignature(hashValue, signature)

def validateDataSignature(data, signature, cert=None, hashFun=None, pubKey=None):
	'''
		return whether <signature> is <hashFun>(<data>) signed using the key corresponding to <pubKey>

		if any of {<hashFun>, <pubKey>} is not provided, it will be found in <cert>.data
	'''
	if hashFun is None:
		hashFun = cert.data.hashFun
	if pubKey is None:
		pubKey = cert.data.publicKey
	hashValue = signed_data.SignedData.hashData(data, hashFun)
	return validateHashSignature(hashValue, signature, pubKey)

def validateCertificate(cert):
	'''
		return whether <cert>.data was signed using the key corresponding to <cert>.cert

		in the case of self-signed certificate, return whether it was signed using its own private key
	'''
	if not isinstance(cert, certificate.Certificate):
		raise TypeError('\'{}\' is not a valid certificate'.format(cert))
	if cert.isSelfsigned():
		ca_cert = cert
	else:
		ca_cert = cert.cert
	return validateDataSignature(cert.data, cert.signature, ca_cert)

def validateCertificateWithChain(cert):
	'''
		return a pair:
			[0] whether this certificate and whole its chain is valid
			[1] list of boolean values meaning:
				[0] whether this certificate is valid
				...
				[end] whether the self-signed root certificate is valid
	'''
	results = []
	total = True
	while True:
		r = validateCertificate(cert)
		results.append(r)
		total &= r
		if cert.isSelfsigned():
			break
		cert = cert.cert
	return (total, results)

def validateSignedData(data):
	'''
		return whether <data>.data was signed using the key corresponding to <data>.cert

		in the case of self-signed certificate, return whether it was signed using its own private key
	'''
	if not isinstance(data, signed_data.SignedData):
		raise TypeError('\'{}\' is not a valid SignedData'.format(data))
	if isinstance(data, certificate.Certificate):
		return validateCertificate(data)
	return validateDataSignature(data.data, data.signature, data.cert)

def validateSignedDataWithChain(data):
	'''
		return a pair:
			[0] whether this SignedData, its certificate certificate and whole its chain is valid
			[1] list of boolean values meaning:
				[0] whether the signature of this data is valid
				[1] whether its certificate is valid
				...
				[end] whether the self-signed root certificate is valid
	'''
	if isinstance(data, certificate.Certificate):
		return validateCertificateWithChain(data)
	total = validateSignedData(data)
	(t2, r2) = validateCertificateWithChain(data.cert)
	results = [total] + r2
	total &= t2
	return (total, results)
