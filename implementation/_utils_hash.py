import hashlib

def doHash(toBeHashed, hashFun='MD5'):
	hashes = {
		'MD5'    : (lambda x: hashlib.md5(x)),
		'SHA1'   : (lambda x: hashlib.sha1(x)),
		'SHA256' : (lambda x: hashlib.sha256(x)),
		'SHA512' : (lambda x: hashlib.sha512(x))
	}
	fun = hashes.get(hashFun.upper(), NotImplemented)
	if fun is NotImplemented:
		raise NotImplementedError('Unknown hash - identified by \'{}\''.format(hashFun))
	hexhash = fun(toBeHashed).hexdigest()
	return long(hexhash, 16)
