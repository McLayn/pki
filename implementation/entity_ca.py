import entity
import certificate
import certificate_request
import certificate_response
import validator

class CertificationAuthority(entity.Entity):
	'''
		Represents a CA in PKI.

		Extends Entity adding field:
		+ lastSerial (int) = serial number of the last issued certificate

		self.listOfIssued is not threadsafe. It will not contain selfsigned certificate. 
		The first serial number is 1.
	'''

	def __init__(self, cert=None, privateKey=None, whoami=None):
		'''
			At least one of <whoami> or <cert> should be supplied.
		'''
		super(CertificationAuthority, self).__init__(cert, privateKey, whoami)
		self.listOfIssued = []

	def processRequest(self, request):
		'''
			process: CertRequest -> CertResponse

			Is not thread-safe
		'''
		if not isinstance(request, certificate_request.CertRequest):
			return certificate_response.CertResponse.newFailedResponse(
				'Invalid request format.'
				)
		if not validator.validateCertificate(request):
			return certificate_response.CertResponse.newFailedResponse(
				'Request was not self-signed with the claimed key.'
				)
		rd = request.data
		data = certificate.Cert_Data(self.getLastSerial() + 1, rd.subject, rd.publicKey, rd.hashFun)
		result = self.signData(data, certificate.Certificate)
		self.listOfIssued.append(result)
		return certificate_response.CertResponse.newSuccessResponse(result)

	def getLastSerial(self):
		return len(self.listOfIssued)
