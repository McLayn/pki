from _utils_format import *
from _utils_hash import *
import collections
import signed_data
import key_asym


Cert_Data = collections.namedtuple("Cert_Data", ["serial", "subject", "publicKey", "hashFun"])



class Certificate(signed_data.SignedData):
	'''
		Certificate for a public key used for asymmetric cryptography

		immutable iff
		- <serial> is immutable and
		- <subject> is immutable and
		- <publicKey> is immutable and
		- <hashFun> is immutable
	'''

	def __new__(cls, data, ca_cert, signature):
		'''
			For a new selfsigned certificate use: 
			Certificate.newSelfsigned(serialNumber, subjectIdentity, privateKey)
		'''
		import signature_object
		import identity
		if not isinstance(data, Cert_Data):
			raise TypeError('\'{}\' is not valid Cert_Data'.format(data))

		if not isinstance(data.serial, (int, long)):
			raise TypeError('serial=\'{}\' is not a number'.format(data.serial))
		if not isinstance(data.subject, identity.Identity):
			raise TypeError('\'{}\' is not a valid identity'.format(data.subject))
		if not isinstance(data.publicKey, key_asym.Key_Public):
			raise TypeError('\'{}\' is not a valid public key'.format(data.publicKey))
		if not isinstance(data.hashFun, (str, unicode)):
			raise TypeError('\'{}\' is not a valid hash function'.format(data.hashFun))

		if not isinstance(ca_cert, Certificate):
			if not (ca_cert is None):
				raise TypeError('\'{}\' is not a valid certificate'.format(ca_cert))

		if not isinstance(signature, signature_object.Signature):
			raise TypeError('\'{}\' is not a valid signature object'.format(signature))

		return super(Certificate, cls).__new__(cls, data, ca_cert, signature)


	def __repr__(self):
		fields = [
			[1, 'subject', self.data.subject],
			[0, 'signature hash algorithm', self.data.hashFun],
			[1, 'public key', self.data.publicKey]
			]
		if self.isSelfsigned():
			# selfsigned
			fields[0][1] = 'SELFSIGNED by subject'
		else:
			fields += [
				[0, 'serial number', self.data.serial],
				[1, 'issuer certificate', self.cert]
				]
		return make_repr(fields)

	def isSelfsigned(self):
		return (self.cert is None)

	@staticmethod
	def newSelfsigned(serial, subject, privkey, hashFun):
		'''
			return a new self-signed certificate
		'''
		if not isinstance(privkey, key_asym.Key_Private):
			raise TypeError('\'{}\' is not a valid private key'.format(privkey))
		data = Cert_Data(serial, subject, privkey.publicKey, hashFun)
		hashValue = signed_data.SignedData.hashData(data, hashFun)
		signature = privkey.sign(hashValue)
		return Certificate(data, None, signature)
