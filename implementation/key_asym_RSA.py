import collections
import key_asym



RSA_PublicKey_Fields = collections.namedtuple("RSA_PublicKey_Fields", ["e", "N"])
RSA_PrivateKey_Fields = collections.namedtuple("RSA_PrivateKey_Fields", ["d"])



class RSA_PublicKey(key_asym.Key_Public):
	'''
		RSA public key container
	'''

	def __new__(cls, e, N):
		import cipher_asym_RSA
		if not isinstance(e, (int, long)):
			raise TypeError('e=\'{}\' is not a number'.format(e))
		if not isinstance(N, (int, long)):
			raise TypeError('N=\'{}\' is not a number'.format(N))
		return super(RSA_PublicKey, cls).__new__(cls, cipher_asym_RSA.RSA, RSA_PublicKey_Fields(e, N))



class RSA_PrivateKey(key_asym.Key_Private):
	'''
		RSA private key container
	'''

	def __new__(cls, d, publicKey):
		if not isinstance(d, (int, long)):
			raise TypeError('d=\'{}\' is not a number'.format(d))
		if not isinstance(publicKey, RSA_PublicKey):
			raise TypeError('\'{}\' is not a valid public key'.format(publicKey))
		return super(RSA_PrivateKey, cls).__new__(cls, publicKey, RSA_PrivateKey_Fields(d))
