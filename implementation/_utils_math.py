import os

def secureRandom(top):
	'''
		return cryptographically secure random number from [ 0, 1, ..., top-1 ]
	'''
	assert (top > 0)
	length = len(bin(top)) # = 2 + length of <top> in binary
	# generate <length> Bytes of <random> (that is 8 times more than what we need)
	rand = long(os.urandom(length).encode('hex'), 16)
	return (rand % top)

def secureRandrange(bot, top):
	'''
		return cryptographically secure random number from [ bot, bot+1, ..., top-1 ]
	'''
	assert (bot < top)
	return (bot + secureRandom(top - bot))

def isPrime(num, accuracy=80):
	'''
		Rabin_Miller

		with low accuracy there might be false positives
	'''
	if not isinstance(num, (int, long)):
		raise TypeError('\'{}\' is not a number'.format(num))

	if (num <= 1):
		return False
	if (num <= 3):
		return True
	if ((num & 1) == 0):
		return False

	# num - 1 = d * 2^r
	d = num - 1
	r = 0
	while ((d & 1) == 0):
		d /= 2
		r += 1

	def test():
		a = secureRandrange(2, num - 1)
		x = pow(a, d, num)
		if ((x == 1) | (x == num - 1)):
			return None
		for i in xrange(r - 1):
			x = pow(x, 2, num)
			if (x == 1):
				return False
			if (x == num - 1):
				return None
		return False

	for k in xrange(accuracy):
		result = test()
		if (result is not None):
			return result
	return True

def getLargePrime(bitlength):
	'''
		Note: This generator uses os.urandom() to get random numbers.
	'''
	if not isinstance(bitlength, (int, long)):
		raise TypeError('\'{}\' is not a number'.format(bitlength))
	if (bitlength < 8):
		raise ValueError('\'{}\' is too small'.format(bitlength))
	while True:
		n = secureRandrange(pow(2, bitlength - 1), pow(2, bitlength))
		if isPrime(n):
			return n

def gcd(a, b):
	'''
		get the greatest common divisor
	'''
	if not isinstance(a, (int, long)):
		raise TypeError('\'{}\' is not a number'.format(a))
	if (a <= 0):
		raise ValueError('\'{}\' is not a positive number'.format(a))

	if not isinstance(b, (int, long)):
		raise TypeError('\'{}\' is not a number'.format(b))
	if (b <= 0):
		raise ValueError('\'{}\' is not a positive number'.format(b))

	while (b > 0):
		(a, b) = (b, a % b)
	return a

def lcm(a, b):
	'''
		get the least common multiple
	'''
	return (a * b / gcd(a, b))

def egcd(a, b):
	'''
		get the greatest common divisor and Bezout Numbers
		return (g, x, y) such that GCD(a, b) == x*a + y*b
	'''
	if not isinstance(a, (int, long)):
		raise TypeError('\'{}\' is not a number'.format(a))
	if (a <= 0):
		raise ValueError('\'{}\' is not a positive number'.format(a))

	if not isinstance(b, (int, long)):
		raise TypeError('\'{}\' is not a number'.format(b))
	if (b <= 0):
		raise ValueError('\'{}\' is not a positive number'.format(b))

	x_prev = 1
	x_next = 0
	y_prev = 0
	y_next = 1
	while (b != 0):
		q = a / b
		(a, b) = (b, a % b)
		(x_prev, x_next) = (x_next, x_prev - q * x_next)
		(y_prev, y_next) = (y_next, y_prev - q * y_next)
	return  (a, x_prev, y_prev)

def invmod(a, n):
	'''
		get (a^{-1} mod n)
		returns None when <a> can't be inverted modulo <n>
	'''
	a %= n
	(g, x, y) = egcd(a, n)
	if (g == 1):
		return (x % n)
	else:
		return None
