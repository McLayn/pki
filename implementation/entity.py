import signed_data
import certificate
import certificate_request
from _utils_format import make_repr
from _utils_math import secureRandom



class Entity(object):
	'''
		Represents an entity in PKI.

		Fields:
		+ whoami (Identity)        = identity of this entity
		+ cert (Certificate)       = certificate of this entity
		+ privateKey (Key_Private) = private key of this entity
	'''

	def __init__(self, cert=None, privateKey=None, whoami=None):
		'''
			At least one of <whoami> or <cert> should be supplied.
		'''
		self.cert = cert
		self.privateKey = privateKey
		if whoami is None:
			whoami = cert.data.subject
		self.whoami = whoami

	def signData(self, data, resultClass=signed_data.SignedData):
		'''
			Create a new SignedData of class <resultClass> using this key
		'''
		if not issubclass(resultClass, signed_data.SignedData):
			raise TypeError('\'{}\' is not a valid SignedData class'.format(resultClass))
		signatureValue = self.privateKey.hashAndSign(data, self.cert.data.hashFun)
		return resultClass(data, self.cert, signatureValue)

	def getNewCert(self, ca, hashFun='SHA256', key=None):
		'''
			Obtain a new certificate for <key>.publicKey signed by <ca>.
			For selfsigned put <ca>=None.
			To keep the previous key, put <key>=None.

			returns a string containing failure info
			(or '' on success)
		'''
		if key is None:
			key = self.privateKey
		if ca is None:
			# selfsigned
			self.privateKey = key
			self.cert = certificate.Certificate.newSelfsigned(0, self.whoami, key, hashFun)
			return ''

		rand = secureRandom(1 << 512)
		data = certificate_request.Request_Data(rand, self.whoami, key.publicKey, hashFun)
		signature = key.hashAndSign(data, hashFun)
		req = certificate_request.CertRequest(data, signature)
		resp = ca.processRequest(req)
		if resp.isFailed():
			return resp.info
		else:
			self.privateKey = key
			self.cert = resp.cert
			return ''

	def getNewKeyAndCert(self, ca, cryptosystem, bitlength, hashFun='SHA256'):
		'''
			Obtain a new key and corresponding certificate signed by <ca>.
			The new key will be for <cryptosystem> with complexity <bitlength>.
			For selfsigned certificate put <ca>=None.

			returns a string containing failure info
			(or '' on success)
		'''
		key = cryptosystem.getNewKey(bitlength)
		return self.getNewCert(ca, hashFun, key)
