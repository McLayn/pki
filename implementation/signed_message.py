from _utils_format import *
from _utils_hash import *
import signed_data



class SignedMessage(signed_data.SignedData):
	'''
		A message signed using asymmetric cryptography
	'''
	
	def __new__(cls, text, cert, signature):
		import signature_object
		import certificate
		if not isinstance(text, (str, unicode)):
			raise TypeError('\'{}\' is not a string'.format(text))
		if not isinstance(cert, certificate.Certificate):
			raise TypeError('\'{}\' is not a valid certificate'.format(cert))
		if not isinstance(signature, signature_object.Signature):
			raise TypeError('\'{}\' is not a valid signature object'.format(signature))

		return super(SignedMessage, cls).__new__(cls, text, cert, signature)
