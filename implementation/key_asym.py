from _utils_format import *
from _utils_hash import *
import collections
import signed_data



Key_Public_WithFields = collections.namedtuple("Key_Public_WithFields", ["cryptosystem", "fields"])
Key_Private_WithFields = collections.namedtuple("Key_Private_WithFields", ["publicKey", "fields"])



class Key_Public(Key_Public_WithFields):
	'''
		Abstract public key container

		immutable iff
		- <cryptosystem> is immutable and
		- <fields> is immutable
	'''

	def __new__(cls, crypto, fields):
		import cipher_asym
		if not issubclass(crypto, cipher_asym.AsymmetricCipher):
			raise TypeError('\'{}\' is not a valid cryptosystem'.format(crypto))
		return super(Key_Public, cls).__new__(cls, crypto, fields)

	def __repr__(self):
		outfields = [(0, 'cryptosystem', self.cryptosystem.getLabel())]
		for item in self.fields._fields:
			value = self.fields._asdict()[item]
			outfields.append((0, item, value))
		return make_repr(outfields)

	def __hash__(self):
		return doHash(self.__repr__())

	def encrypt(self, message):
		'''
			encrypt a message using this key
		'''
		return self.cryptosystem.encrypt(message, self)

	def validateSignature(self, message, signatureValue):
		'''
			validate a signature of a message using this key
		'''
		return self.cryptosystem.validateSignature(message, self, signatureValue)

	def hashAndValidate(self, message, hashFun, signatureValue):
		'''
			validate whether <signatureValue> is a valid signature of <hashFun>(<message>)
		'''
		hashValue = signed_data.SignedData.hashData(message, hashFun)
		return self.validateSignature(hashValue, signatureValue)



class Key_Private(Key_Private_WithFields):
	'''
		Abstract private key container

		immutable iff
		- <publicKey> is immutable and
		- <fields> is immutable
	'''

	def __new__(cls, publicKey, fields):
		if not isinstance(publicKey, Key_Public):
			raise TypeError('\'{}\' is not a valid public key'.format(publicKey))
		return super(Key_Private, cls).__new__(cls, publicKey, fields)

	def __repr__(self):
		outfields = []
		for item in self.fields._fields:
			value = self.fields._asdict()[item]
			outfields.append((0, item, value))
		outfields.append((1, 'publicKey', str(self.publicKey)))
		return make_repr(outfields)

	def __hash__(self):
		return doHash(self.__repr__())

	def decrypt(self, message):
		'''
			decrypt a message using this key
		'''
		return self.publicKey.cryptosystem.decrypt(message, self)

	def sign(self, message):
		'''
			sign a message using this key
		'''
		return self.publicKey.cryptosystem.sign(message, self)

	def hashAndSign(self, message, hashFun):
		'''
			sign <hashFun>(<message>) using this key
		'''
		hashValue = signed_data.SignedData.hashData(message, hashFun)
		return self.sign(hashValue)
