def make_repr(data):
	'''
		for (0, name, value) in <data> put:
			<name> = (<value>)

		for (1, name, value) in <data> put:
			<name> = (
				<value> shifted right by 1 tab
			)
	'''
	result = ''
	for tup in data:
		(nl, name, value) = tup
		if (nl == 0):
			result += '{} = ('.format(name)
			something = False
			for line in str(value).splitlines():
				result += '{}; '.format(line.strip())
				something = True
			if something:
				result = result[:-2]
			result += ')\n'
		elif (nl == 1):
			result += '{} = (\n'.format(name)
			for line in str(value).splitlines():
				result += '\t{}\n'.format(line)
			result += '\t)\n'
	return result[:-1]
