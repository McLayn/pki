from _utils_format import *
from _utils_hash import *
import collections



Data_With_Signature = collections.namedtuple("Data_With_Signature", ["data", "cert", "signature"])



class SignedData(Data_With_Signature):
	'''
		Container for data and its signature and the corresponding certificate

		immutable iff
		- <data> is immutable and
		- <cert> is immutable
		- <signature> is immutable
	'''
	
	def __new__(cls, data, cert, signature):
		import signature_object
		import certificate
		if not isinstance(cert, certificate.Certificate):
			if not (cert is None):
				raise TypeError('\'{}\' is not a valid certificate'.format(cert))
		if not isinstance(signature, signature_object.Signature):
			raise TypeError('\'{}\' is not a valid signature object'.format(signature))
		return super(SignedData, cls).__new__(cls, data, cert, signature)

	def __repr__(self):
		fields = [
			[1, 'data', self.data],
			[1, 'signature', self.signature],
			[1, 'cert', self.cert]
			]
		return make_repr(fields)

	def __hash__(self):
		return doHash(self.__repr__())

	@staticmethod
	def hashData(data, hashFun):
		'''
			hash <data> using <hashFun>

			return <hashFun>(<data>)
		'''
		data = str(data)
		return doHash(data, hashFun)

	def getDataHash(self, hashFun):
		'''
			hash <self>.data using <hashFun>

			return <hashFun>(<self>.data)
		'''
		return self.__class__.hashData(self.data, hashFun)
