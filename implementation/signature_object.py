from _utils_format import *
from _utils_hash import *
import collections



Signature_With_Fields = collections.namedtuple("Signature_With_Fields", ["signature_value"])



class Signature(Signature_With_Fields):
	'''
		Abstract container for a signature

		immutable iff
		- <signature_value> is immutable
	'''
	
	def __new__(cls, sign_val):
		return super(Signature, cls).__new__(cls, sign_val)

	def __repr__(self):
		fields = [(0, 'signature value', str(self.signature_value))]
		return make_repr(fields)

	def __hash__(self):
		return doHash(self.__repr__())
