import certificate



Request_Data = certificate.Cert_Data



class CertRequest(certificate.Certificate):
	'''
		Represents a certificate request.
		The request is similar to a self-signed certificate.
		The self-signature prooves that the requesting entity has the private key corresponding to this certificate.
	'''

	def __new__(cls, data, signature):
		'''
			<data>.serial is ment to be a random number
		'''
		if not isinstance(data, Request_Data):
			raise TypeError('\'{}\' is not valid Request_Data'.format(data))
		return super(CertRequest, cls).__new__(cls, data, None, signature)
