class AsymmetricCipher(object):
	'''
		Abstract asymmetric cipher
	'''
	
	def __new__(cls):
		raise NotImplementedError('This is a static class. Sorry.')

	@classmethod
	def getLabel(cls):
		return cls.__name__

	# Encryption / Decryption:

	@staticmethod
	def encrypt(message, publicKey):
		raise NotImplementedError()

	@staticmethod
	def decrypt(message, privateKey):
		raise NotImplementedError()

	# Signature / Validation:

	@staticmethod
	def sign(message, privateKey):
		raise NotImplementedError()

	@staticmethod
	def validateSignature(message, publicKey, signatureValue):
		raise NotImplementedError()

	@staticmethod
	def corruptSignature(signatureValue):
		'''
			Changes a signature-value to another valid signature-value.

			For validator testing.
		'''
		raise NotImplementedError()

	# Generating keys:

	@staticmethod
	def getNewKey(bitlength):
		raise NotImplementedError()
