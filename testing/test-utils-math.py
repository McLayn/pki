import sys
impl_path = '..\\implementation'
if impl_path not in sys.path:
	sys.path.append(impl_path)

from _utils_math import *
from time import clock



print('testing isPrime() ...')
assert isPrime(2)
assert isPrime(3)
assert isPrime(5)
assert isPrime(7)
assert isPrime(11)
assert isPrime(65537) # Fermat prime F4 = 2^2^4 + 1
assert isPrime((1 << 61) - 1) # Mersenne prime (2^61 - 1)
assert not isPrime(0)
assert not isPrime(1)
assert not isPrime(-42)
assert not isPrime(-7)
assert not isPrime(42)
assert not isPrime(4)
assert not isPrime(9)
assert not isPrime(666)
assert not isPrime(2465) # Carmichael number
assert not isPrime(6601) # Carmichael number
assert not isPrime(8911) # Carmichael number
assert not isPrime(278545) # Carmichael number
assert not isPrime(9746347772161) # Carmichael number
assert not isPrime(113163847744010117850975876556459972620164594550699898644277919959952851410820133308822668753104903857197745791079830432907689646921265684651872664668247220510340826700573438167176996802007023434469300997050280855281177247317423274531613045459266425822820391979218890136948517329743091554130658967174432984439) # composed of two 512-bit primes

print('testing getLargePrime() ...')
numberOfPrimes = 10
try:
	print('press Ctrl+C to skip')
	for i in xrange(numberOfPrimes):
		if (i == 0):
			leng = 512
		elif (i == 1):
			leng = 1024
		elif (i == 2):
			leng = 2048
		else:
			leng = secureRandrange(12, 42)
			leng = leng*leng + secureRandrange(-100, 300)
		print '{}\t{:>8}-bit '.format(numberOfPrimes - i, leng),
		startTime = clock()
		p = getLargePrime(leng)
		print('in {} s'.format(str(clock() - startTime)[:5]))
		assert isPrime(p)
		assert ((p >> (leng-1)) == 1)
except KeyboardInterrupt:
	print('\n\n\tSKIPPING.\n')

print('testing gcd(), lcm(), egcd() ...')
for i in xrange(32):
	a = secureRandrange(100, 500)
	if (i == 0):
		b = 3 * a
	else:
		b = secureRandrange(100, 700)

	found = False
	for g in reversed(xrange(min(a, b) + 1)):
		if (((a % g) == 0) & ((b % g) == 0)):
			found = True
			assert (g == gcd(a, b))
			assert (g == gcd(b, a))
			(z1, x1, y1) = egcd(a, b)
			(z2, x2, y2) = egcd(b, a)
			assert (g == z1)
			assert (g == z2)
			assert (z1 == a * x1 + b * y1)
			assert (z2 == b * x2 + a * y2)
			break
	assert found

	l = max(a, b)
	while True:
		if (((l % a) == 0) & ((l % b) == 0)):
			assert (l == lcm(a, b))
			assert (l == lcm(b, a))
			break
		l += 1

print('testing invmod() ...')
for i in xrange(32):
	a = 1
	n = 1
	for j in xrange(4):
		a *= getLargePrime(10)
		n *= getLargePrime(10)
	c = invmod(a, n)
	if (gcd(a, n) == 1):
		assert (((a * c) % n) == 1)
	else:
		assert(c == None)
