import sys
impl_path = '..\\implementation'
if impl_path not in sys.path:
	sys.path.append(impl_path)

from signed_message import SignedMessage
from certificate import Certificate, Cert_Data
from identity import Identity
from cipher_asym_RSA import RSA



print('testing signature of certificates and messages ...')

ca_private = RSA.getNewKey(1024)
issuer = Identity('Boss Nass', 'natural', 'Naboo', moreInfo='like a boss')
ca_cert = Certificate.newSelfsigned(1, issuer, ca_private, 'SHA256')

assert ca_cert.data.publicKey.hashAndValidate(ca_cert.data, ca_cert.data.hashFun, ca_cert.signature)

subject = Identity('Jar Jar Binks', 'natural', 'Naboo', 'binks@jar.jar', web='www.mesa.jar.jar')
private = RSA.getNewKey(1024)
data = Cert_Data(10842, subject, private.publicKey, 'SHA1')
signature = ca_private.hashAndSign(data, ca_cert.data.hashFun)
cert = Certificate(data, ca_cert, signature)

assert cert.cert.data.publicKey.hashAndValidate(cert.data, cert.cert.data.hashFun, cert.signature)

data = 'Hello there!'
signature = private.hashAndSign(data, cert.data.hashFun)
sm = SignedMessage(data, cert, signature)

assert cert.data.publicKey.hashAndValidate(sm.data, sm.cert.data.hashFun, sm.signature)
