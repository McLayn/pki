import sys
impl_path = '..\\implementation'
if impl_path not in sys.path:
	sys.path.append(impl_path)

from certificate import Certificate, Cert_Data
from identity import Identity
from cipher_asym_RSA import RSA



print('testing certificate creation and printing ...')

ca_private = RSA.getNewKey(1024)
issuer = Identity('Boss Nass', 'natural', 'Naboo', moreInfo='like a boss')
ca_cert = Certificate.newSelfsigned(1, issuer, ca_private, 'SHA256')

subject = Identity('Jar Jar Binks', 'natural', 'Naboo', 'binks@jar.jar', web='www.mesa.jar.jar')
private = RSA.getNewKey(1024)
data = Cert_Data(10842, subject, private.publicKey, 'SHA1')
signature = ca_private.sign(hash(data))
c = Certificate(data, ca_cert, signature)

assert ca_cert.isSelfsigned()
print ca_cert

print ''

assert not c.isSelfsigned()
print c
