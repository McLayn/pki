import sys
impl_path = '..\\implementation'
if impl_path not in sys.path:
	sys.path.append(impl_path)

from os import listdir
from importlib import import_module


# run all *.py scripts in this dir
for testScript in listdir('./'):
	# skip self:
	if (testScript == sys.argv[0]):
		continue
	# skip others than *.py
	if not (testScript.endswith('.py')):
		continue

	print('################################################################')
	print('running {} ...'.format(testScript))
	print('################################################################')
	import_module(testScript[:-3])
	print('################################################################')



print(
	'\n' +
	'Well done. ' +
	'Here come the test results: ' +
	'You are a horrible person. ' +
	'\n' +
	'That\'s what it says: a horrible person. ' +
	'We weren\'t even testing for that. ' +
	'\n\n\t ~ GLaDOS'
	)
