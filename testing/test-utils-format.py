import sys
impl_path = '..\\implementation'
if impl_path not in sys.path:
	sys.path.append(impl_path)

from _utils_format import *



print('testing make_repr() ...')

fields = [(1,'','')]
expected = ' = (\n\t)'
assert (make_repr(fields) == expected)

fields = [(0,'','')]
expected = ' = ()'
assert (make_repr(fields) == expected)


fields = []
expected = ''
assert (make_repr(fields) == expected)

fields.append((0, 'a 42', 'here we go'))
expected += 'a 42 = (here we go)'
assert (make_repr(fields) == expected)

fields.append((1, 'bb 42', 'here we go'))
expected += '\nbb 42 = (\n\there we go\n\t)'
assert (make_repr(fields) == expected)

fields.append((0, 'c 42', 'here\nwe go\nand also here'))
expected += '\nc 42 = (here; we go; and also here)'
assert (make_repr(fields) == expected)

fields.append((1, 'ddd 42', 'here\nwe go\nand also here'))
expected += '\nddd 42 = (\n\there\n\twe go\n\tand also here\n\t)'
assert (make_repr(fields) == expected)

fields.append((1, '42', '\there\n\twe go\nand also here'))
expected += '\n42 = (\n\t\there\n\t\twe go\n\tand also here\n\t)'
assert (make_repr(fields) == expected)

print(make_repr([(1, 'tested on this', expected), (0, 'and this', '42\n42\nabc def')]))
