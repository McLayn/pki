import sys
impl_path = '..\\implementation'
if impl_path not in sys.path:
	sys.path.append(impl_path)

from identity import Identity
from entity import Entity
from entity_ca import CertificationAuthority
from cipher_asym_RSA import RSA
from signed_message import SignedMessage
import validator



print('testing entity and certificate requests ...')

h = ['MD5', 'SHA1', 'SHA256', 'SHA512']

root = Identity('the sea of Naboo', 'legal', 'Naboo')
issuer = Identity('Boss Nass', 'natural', 'Naboo', moreInfo='like a boss')
endUser = Identity('Jar Jar Binks', 'natural', 'Naboo', 'binks@jar.jar', web='www.mesa.jar.jar')

ca_r = CertificationAuthority(whoami=root)
result = ca_r.getNewKeyAndCert(None, RSA, 2048, h[3])
assert (result == '')

ca_r_noc = 0
ca_i_noc = 0

ca_i = CertificationAuthority(whoami=issuer)
result = ca_i.getNewKeyAndCert(ca_r, RSA, 512, h[0])
ca_r_noc += 1
assert (result == '')
assert (ca_i.cert.data.hashFun == h[0])
prev_cert = ca_i.cert.data
assert (prev_cert.serial == 1)
result = ca_i.getNewKeyAndCert(ca_r, RSA, 2048, h[1])
ca_r_noc += 1
assert (result == '')
assert (ca_i.cert.data.serial == prev_cert.serial + 1)
assert (ca_i.cert.data.hashFun == h[1])
prev_cert = ca_i.cert.data
result = ca_i.getNewCert(ca=ca_r, hashFun=h[2])
ca_r_noc += 1
assert (result == '')
assert (ca_i.cert.data.serial == prev_cert.serial + 1)
assert (ca_i.cert.data.hashFun == h[2])
assert (ca_i.cert.data.publicKey == prev_cert.publicKey)
assert (ca_i.cert.data.publicKey == ca_i.privateKey.publicKey)

user = Entity(whoami=endUser)
result = user.getNewKeyAndCert(ca_i, RSA, 1024, 'SHA1')
ca_i_noc += 1
assert (result == '')

mess = user.signData('Me\'sa Jar Jar.\nMe\'sa... clumsy...', SignedMessage)
print(mess)
(total, partials) = validator.validateSignedDataWithChain(mess)
assert total
assert (partials == [True, True, True, True])

assert (ca_r.getLastSerial() == ca_r_noc)
assert (ca_i.getLastSerial() == ca_i_noc)
assert (ca_r.listOfIssued[ca_r.getLastSerial() - 2] != ca_i.cert)
assert (ca_r.listOfIssued[ca_r.getLastSerial() - 1] == ca_i.cert)
assert (ca_i.listOfIssued[ca_i.getLastSerial() - 1] == user.cert)
