import sys
impl_path = '..\\implementation'
if impl_path not in sys.path:
	sys.path.append(impl_path)

from certificate import Certificate, Cert_Data
from identity import Identity
from cipher_asym_RSA import RSA
from signature_RSA import Signature_RSA



print('testing hashes of certificates ...')

ca_private = RSA.getNewKey(1024)
user_private = RSA.getNewKey(1024)

issuer1 = Identity('Boss Nass', 'natural', 'Naboo', moreInfo='like a boss')
issuer2 = Identity('Boss Nass', 'natural', 'Naboo', moreInfo='like a boss')
ca_cert1 = Certificate.newSelfsigned(1, issuer1, ca_private, 'SHA512')
ca_cert2 = Certificate.newSelfsigned(1, issuer2, ca_private, 'SHA512')

# with some overthinking and overoptimization, these might be equal
# if so, the next block of assertions would be meaningless
assert (id(issuer1) != id(issuer2))
assert (id(ca_cert1) != id(ca_cert2))

assert (hash(issuer1) == hash(issuer2))
assert (issuer1 == issuer2)
assert (ca_cert1 == ca_cert2)
assert (hash(ca_cert1) == hash(ca_cert2))
assert (ca_cert1 == ca_cert2)

subject1 = Identity('Jar Jar Binks', 'natural', 'Naboo', 'binks@jar.jar', web='www.mesa.jar.jar')
subject2 = Identity('Jar Jar Binks', 'natural', 'Naboo', 'binks@jar.jar', web='www.mesa.jar.jar')
data1 = Cert_Data(10842, subject1, user_private.publicKey, 'SHA1')
data2 = Cert_Data(10842, subject1, user_private.publicKey, 'SHA1')
signature1 = ca_private.sign(hash(data1))
signature2 = ca_private.sign(hash(data2))
c11 = Certificate(data1, ca_cert1, signature1)
c12 = Certificate(data1, ca_cert2, signature1)
c21 = Certificate(data2, ca_cert1, signature2)
c22 = Certificate(data2, ca_cert2, signature2)

# with some overthinking and overoptimization, these might be equal
# if so, the next block of assertions would be meaningless
assert (id(c11) != id(c12))
assert (id(c21) != id(c22))
assert (id(c11) != id(c21))

assert (hash(c11) == hash(c12))
assert (hash(c21) == hash(c22))
assert (hash(c11) == hash(c21))
assert (c11 == c12)
assert (c21 == c22)
assert (c11 == c21)
