import sys
impl_path = '..\\implementation'
if impl_path not in sys.path:
	sys.path.append(impl_path)

from cipher_asym_RSA import RSA
from signature_RSA import Signature_RSA



private = RSA.getNewKey(1024)
public = private.publicKey

print('{}\n'.format(public))
print('{}\n'.format(private))

print('Testing raw functions ...')
test_message = 42
encrypted = public.cryptosystem.encrypt(test_message, public)
decrypted = private.publicKey.cryptosystem.decrypt(encrypted, private)
assert (decrypted == test_message)
signed = private.publicKey.cryptosystem.sign(test_message, private)
validated = public.cryptosystem.validateSignature(test_message, public, signed)
assert validated

print('Testing simplified functions ...')
encrypted = public.encrypt(test_message)
decrypted = private.decrypt(encrypted)
assert (decrypted == test_message)
signed = private.sign(test_message)
validated = public.validateSignature(test_message, signed)
assert validated

print('Testing high value ...')
huge = (test_message << 5000)
result = False
try:
	encrypted = public.encrypt(huge)
except ValueError:
	result = True
assert result
signed = private.sign(huge)
validated = public.validateSignature(huge, signed)
assert validated

print('Testing changed values ...')
encrypted = public.encrypt(test_message) ^ 1
decrypted = private.decrypt(encrypted)
assert (decrypted != test_message)
signed = private.sign(test_message)
signed = Signature_RSA(signed.signature_value ^ 1)
validated = public.validateSignature(test_message, signed)
assert not validated
