import sys
impl_path = '..\\implementation'
if impl_path not in sys.path:
	sys.path.append(impl_path)

from signed_message import SignedMessage
from certificate import Certificate, Cert_Data
from identity import Identity
from cipher_asym_RSA import RSA
import validator



print('testing validator (nice cases) ...')

ca_private = RSA.getNewKey(1024)
issuer = Identity('Boss Nass', 'natural', 'Naboo', moreInfo='like a boss')
ca_cert = Certificate.newSelfsigned(1, issuer, ca_private, 'SHA256')

assert validator.validateCertificate(ca_cert)
assert validator.validateSignedData(ca_cert)
(total, results) = validator.validateCertificateWithChain(ca_cert)
assert total
assert (results == [True])
(total, results) = validator.validateSignedDataWithChain(ca_cert)
assert total
assert (results == [True])

subject = Identity('Jar Jar Binks', 'natural', 'Naboo', 'binks@jar.jar', web='www.mesa.jar.jar')
private = RSA.getNewKey(1024)
data = Cert_Data(10842, subject, private.publicKey, 'SHA1')
signature = ca_private.hashAndSign(data, ca_cert.data.hashFun)
cert = Certificate(data, ca_cert, signature)

assert validator.validateCertificate(cert)
assert validator.validateSignedData(cert)
(total, results) = validator.validateCertificateWithChain(cert)
assert total
assert (results == [True, True])
(total, results) = validator.validateSignedDataWithChain(cert)
assert total
assert (results == [True, True])

text = 'Hello there!\n42'
signature = private.hashAndSign(text, cert.data.hashFun)
sm = SignedMessage(text, cert, signature)

assert validator.validateSignedData(sm)
(total, results) = validator.validateSignedDataWithChain(sm)
assert total
assert (results == [True, True, True])



print('testing validator (bad cert) ...')

data = Cert_Data(10842, subject, private.publicKey, 'SHA1')
signature = ca_private.hashAndSign(data, ca_cert.data.hashFun)
data = Cert_Data(10842, subject, private.publicKey, 'MD5')
cert = Certificate(data, ca_cert, signature)

assert not validator.validateCertificate(cert)
assert not validator.validateSignedData(cert)
(total, results) = validator.validateCertificateWithChain(cert)
assert not total
assert (results == [False, True])
(total, results) = validator.validateSignedDataWithChain(cert)
assert not total
assert (results == [False, True])

text = 'Hello there!\n42'
signature = private.hashAndSign(text, cert.data.hashFun)
sm = SignedMessage(text, cert, signature)

assert validator.validateSignedData(sm)
(total, results) = validator.validateSignedDataWithChain(sm)
assert  not total
assert (results == [True, False, True])
